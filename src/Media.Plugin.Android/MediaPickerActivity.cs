//
//  Copyright 2011-2013, Xamarin Inc.
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Provider;
using Environment = Android.OS.Environment;
using Path = System.IO.Path;
using Uri = Android.Net.Uri;
using Plugin.Media.Abstractions;
using Android.Net;
using Android.Content.PM;
using System.Collections.Generic;
using Android.Webkit;
using Xamarin.Essentials;

namespace Plugin.Media
{
    public enum MediaFileType
    {
        Image,
        Video,
        Other
    }

    /// <summary>
    /// Picker
    /// </summary>
    [Activity(ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    [Android.Runtime.Preserve(AllMembers = true)]
    public class MediaPickerActivity
        : Activity, Android.Media.MediaScannerConnection.IOnScanCompletedListener
    {
        internal const string ExtraPath = "path";
        internal const string ExtraLocation = "location";
        internal const string ExtraType = "type";
        internal const string ExtraId = "id";
        internal const string ExtraAction = "action";
        internal const string ExtraTasked = "tasked";
        internal const string ExtraSaveToAlbum = "album_save";
        internal const string ExtraFront = "android.intent.extras.CAMERA_FACING";

        internal static event EventHandler<MediaPickedEventArgs> MediaPicked;

        private int id;
        private int front;
        private string title;
        private string description;
        private string type;

        /// <summary>
        /// The user's destination path.
        /// </summary>
        private Uri path;
        private MediaFileType fileType;
        private bool saveToAlbum;
        private string action;

        private int seconds;
        private VideoQuality quality;

        private bool tasked;
        private bool allowMultiple;

        /// <summary>
        /// OnSaved
        /// </summary>
        /// <param name="outState"></param>
        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutBoolean("ran", true);
            outState.PutString(MediaStore.MediaColumns.Title, this.title);
            outState.PutString(MediaStore.Images.ImageColumns.Description, this.description);
            outState.PutInt(ExtraId, this.id);
            outState.PutString(ExtraType, this.type);
            outState.PutString(ExtraAction, this.action);
            outState.PutInt(MediaStore.ExtraDurationLimit, this.seconds);
            outState.PutInt(MediaStore.ExtraVideoQuality, (int)this.quality);
            outState.PutBoolean(ExtraSaveToAlbum, saveToAlbum);
            outState.PutBoolean(ExtraTasked, this.tasked);
            outState.PutInt(ExtraFront, this.front);
            outState.PutBoolean(Intent.ExtraAllowMultiple, this.allowMultiple);

            if (this.path != null)
                outState.PutString(ExtraPath, this.path.Path);

            base.OnSaveInstanceState(outState);
        }

        

        /// <summary>
        /// OnCreate
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Bundle b = (savedInstanceState ?? Intent.Extras);

            bool ran = b.GetBoolean("ran", defaultValue: false);

            this.title = b.GetString(MediaStore.MediaColumns.Title);
            this.description = b.GetString(MediaStore.Images.ImageColumns.Description);

            this.tasked = b.GetBoolean(ExtraTasked);
            this.id = b.GetInt(ExtraId, 0);
            this.type = b.GetString(ExtraType);
            this.front = b.GetInt(ExtraFront);
            if (this.type == "image/*")
                this.fileType = MediaFileType.Image;
            else if (this.type == "video/*")
                this.fileType = MediaFileType.Video;
            else
                this.fileType = MediaFileType.Other;

            this.action = b.GetString(ExtraAction);
            Intent pickIntent = null;
            try
            {
                pickIntent = new Intent(this.action);
                if (this.action == Intent.ActionPick)
                    pickIntent.SetType(type);
                else if (this.action == Intent.ActionGetContent)
                {
                    // Choosing arbitrary files ... see commentary about ACTION_GET_CONTENT with CATEGORY_OPENABLE and MIME type '*/*' : https://developer.android.com/reference/android/content/Intent.html
                    //Uri startDir = Uri.FromFile(new Java.IO.File("/sdcard"));
                    //pickIntent.SetDataAndType(startDir, "*/*");
                    pickIntent.SetType("*/*");
                    pickIntent.AddCategory(Intent.CategoryOpenable);
                }
                else
                {
                    if (this.fileType == MediaFileType.Video)
                    {
                        this.seconds = b.GetInt(MediaStore.ExtraDurationLimit, 0);
                        if (this.seconds != 0)
                            pickIntent.PutExtra(MediaStore.ExtraDurationLimit, seconds);
                    }

                    this.saveToAlbum = b.GetBoolean(ExtraSaveToAlbum);
                    pickIntent.PutExtra(ExtraSaveToAlbum, this.saveToAlbum);

                    this.quality = (VideoQuality)b.GetInt(MediaStore.ExtraVideoQuality, (int)VideoQuality.High);
                    pickIntent.PutExtra(MediaStore.ExtraVideoQuality, GetVideoQuality(this.quality));

                    if (front != 0)
                        pickIntent.PutExtra(ExtraFront, (int)Android.Hardware.CameraFacing.Front);

                    if (!ran)
                    {
                        this.path = GetOutputMediaFile(this, b.GetString(ExtraPath), this.title, this.fileType, false);

                        Touch();

						var targetsNOrNewer = false;

						try
						{
							targetsNOrNewer = (int)Application.Context.ApplicationInfo.TargetSdkVersion >= 24;
						}
						catch(Exception appInfoEx)
						{
							System.Diagnostics.Debug.WriteLine("Unable to get application info for targetSDK, trying to get from package manager: " + appInfoEx);
							targetsNOrNewer = false;

							var appInfo = PackageManager.GetApplicationInfo(Application.Context.PackageName, 0);
							if (appInfo != null)
							{
								targetsNOrNewer = (int)appInfo.TargetSdkVersion >= 24;
							}
						}

						if (targetsNOrNewer && this.path.Scheme == "file")
						{
							var photoURI = FileProvider.GetUriForFile(this,
																	  Application.Context.PackageName + ".fileprovider",
							                                          new Java.IO.File(this.path.Path));

							GrantUriPermissionsForIntent(pickIntent, photoURI);
							pickIntent.PutExtra(MediaStore.ExtraOutput, photoURI);
						}
						else
						{
							pickIntent.PutExtra(MediaStore.ExtraOutput, this.path);
						}
                    }
                    else
                        this.path = Uri.Parse(b.GetString(ExtraPath));
                }

                if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.JellyBeanMr2)
                {
                    this.allowMultiple = b.GetBoolean(Intent.ExtraAllowMultiple);
                    pickIntent.PutExtra(Intent.ExtraAllowMultiple, this.allowMultiple);
                }

                if (!ran)
                    StartActivityForResult(pickIntent, this.id);
            }
            catch (Exception ex)
            {
                OnMediaPicked(new MediaPickedEventArgs(this.id, ex));
                //must finish here because an exception has occured else blank screen
                Finish();
            }
            finally
            {
                if (pickIntent != null)
                    pickIntent.Dispose();
            }
        }

        private void Touch()
        {
            if (this.path.Scheme != "file")
                return;

            var newPath = GetLocalPath(this.path);
            try
            {
                var stream = File.Create(newPath);
                stream.Close();
                stream.Dispose();

            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to create path: " + newPath + " " + ex.Message + "This means you have illegal characters");
                throw ex;
            }
        }

        private void DeleteOutputFile()
        {
            try
            {
                if (this.path?.Scheme != "file")
                    return;

                var localPath = GetLocalPath(this.path);

                if (File.Exists(localPath))
                {
                    File.Delete(localPath);
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to delete file: " + ex.Message);
            }
        }

		private void GrantUriPermissionsForIntent(Intent intent, Uri uri)
		{
			var resInfoList = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			foreach (var resolveInfo in resInfoList)
			{
				var packageName = resolveInfo.ActivityInfo.PackageName;
				GrantUriPermission(packageName, uri, ActivityFlags.GrantWriteUriPermission | ActivityFlags.GrantReadUriPermission);
			}
		}

        internal static Task<MediaPickedEventArgs> GetMediaFileAsync(Context context, int requestCode, string action, MediaFileType fileType, ref Uri path, Uri data, bool saveToAlbum)
        {
            Task<Tuple<string, bool>> pathFuture;

            string originalPath = null;

            if (action != Intent.ActionPick && action != Intent.ActionGetContent)
            {

                originalPath = path.Path;


                // Not all camera apps respect EXTRA_OUTPUT, some will instead
                // return a content or file uri from data.
                if (data != null && data.Path != originalPath)
                {
                    originalPath = data.ToString();
                    string currentPath = path.Path;
                    pathFuture = TryMoveFileAsync(context, data, path, fileType, false).ContinueWith(t =>
                        new Tuple<string, bool>(t.Result ? currentPath : null, false));
                }
                else
                {
                    pathFuture = TaskFromResult(new Tuple<string, bool>(path.Path, false));
                   
                }
            }
            else if (data != null)
            {
                originalPath = data.ToString();
                path = data;
                pathFuture = GetFileForUriAsync(context, path, fileType, false);
            }
            else
                pathFuture = TaskFromResult<Tuple<string, bool>>(null);

            return pathFuture.ContinueWith(t =>
            {
                
                string resultPath = t.Result.Item1;
                var aPath = originalPath;
                if (resultPath != null && File.Exists(t.Result.Item1))
                {
                    var mf = new MediaFile(resultPath, () =>
                      {
                          return File.OpenRead(resultPath);
                      }, albumPath: aPath);
                    return new MediaPickedEventArgs(requestCode, false, mf);
                }
                else
                    return new MediaPickedEventArgs(requestCode, new MediaFileNotFoundException(originalPath));
            });
        }

        bool completed;
        /// <summary>
        /// OnActivity Result
        /// </summary>
        /// <param name="requestCode"></param>
        /// <param name="resultCode"></param>
        /// <param name="data"></param>
        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            completed = true;
            base.OnActivityResult(requestCode, resultCode, data);



            if (this.tasked)
            {

               
                Task<MediaPickedEventArgs> future;

                if (resultCode == Result.Canceled)
                {
                    //delete empty file
                    DeleteOutputFile();

                    future = TaskFromResult(new MediaPickedEventArgs(requestCode, isCanceled: true));

                    Finish();
                    await Task.Delay(50);
                    future.ContinueWith(t => OnMediaPicked(t.Result));
                }
                else
                {
                    ClipData clipData = data?.ClipData;

                    MediaPickedEventArgs e;
                    if (clipData?.ItemCount > 1)
                    {
                        Uri uri = clipData.GetItemAt(0).Uri;
                        e = await GetMediaFileAsync(this, requestCode, this.action, this.fileType, ref this.path, uri, false);
                        IList<MediaFile> allMedia = e.AllMedia;
                        int itemCount = clipData.ItemCount;
                        for (int i = 1; i < itemCount; i++)
                        {
                            uri = clipData.GetItemAt(i).Uri;
                            var f = await GetMediaFileAsync(this, requestCode, this.action, this.fileType, ref this.path, uri, false);
                            allMedia.Add(f.Media);
                        }
                    }
                    else
                    {
                        e = await GetMediaFileAsync(this, requestCode, this.action, this.fileType, ref this.path, (data != null) ? data.Data : null, false);
                    }
                    Finish();
                    await Task.Delay(50);
                    OnMediaPicked(e);

                }
            }
            else
            {
                if (resultCode == Result.Canceled)
                {
                    //delete empty file
                    DeleteOutputFile();

                    SetResult(Result.Canceled);
                }
                else
                {
                    Intent resultData = new Intent();
                    resultData.PutExtra("MediaFile", (data != null) ? data.Data : null);
                    resultData.PutExtra("path", this.path);
                    resultData.PutExtra("fileType", (int)(this.fileType));
                    resultData.PutExtra("action", this.action);
                    resultData.PutExtra(ExtraSaveToAlbum, this.saveToAlbum);
                    SetResult(Result.Ok, resultData);
                }

                Finish();
            }
        }

        public static Task<bool> TryMoveFileAsync(Context context, Uri url, Uri path, MediaFileType fileType, bool saveToAlbum)
        {
            string moveTo = GetLocalPath(path);
            return GetFileForUriAsync(context, url, fileType, false).ContinueWith(t =>
            {
                if (t.Result.Item1 == null)
                    return false;

                try
                {
                    if (url.Scheme == "content")
                        context.ContentResolver.Delete(url, null, null);
                }
                catch(Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Unable to delete content resolver file: " + ex.Message);
                }

                try
                {
                    File.Delete(moveTo);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Unable to delete normal file: " + ex.Message);
                }

                try
                {
                    File.Move(t.Result.Item1, moveTo);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Unable to move files: " + ex.Message);
                }

                return true;
            }, TaskScheduler.Default);
        }

        private static int GetVideoQuality(VideoQuality videoQuality)
        {
            switch (videoQuality)
            {
                case VideoQuality.Medium:
                case VideoQuality.High:
                    return 1;

                default:
                    return 0;
            }
        }

        private static string GetUniquePath(string folder, string name, MediaFileType fileType)
        {
            string ext = Path.GetExtension(name);
            if (ext == String.Empty)
            {
                switch (fileType)
                {
                    case MediaFileType.Image:
                        ext = ".jpg";
                        break;
                    case MediaFileType.Video:
                        ext = ".mp4";
                        break;
                    default:
                        ext = "";
                        break;
                }
            }

            name = Path.GetFileNameWithoutExtension(name);

            string nname = name + ext;
            int i = 1;
            while (File.Exists(Path.Combine(folder, nname)))
                nname = name + "_" + (i++) + ext;

            return Path.Combine(folder, nname);
        }

        public static Uri GetOutputMediaFile(Context context, string subdir, string name, MediaFileType fileType, bool saveToAlbum)
        {
            subdir = subdir ?? String.Empty;

            if (String.IsNullOrWhiteSpace(name))
            {
                string timestamp = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                switch (fileType)
                {
                    case MediaFileType.Image:
                        name = "IMG_" + timestamp + ".jpg";
                        break;
                    case MediaFileType.Video:
                        name = "VID_" + timestamp + ".mp4";
                        break;
                    default:
                        name = "FILE_" + timestamp;
                        break;
                }
                
            }
            string mediaType = null;
            switch (fileType)
            {
                case MediaFileType.Image:
                    mediaType = Environment.DirectoryPictures;
                    break;
                case MediaFileType.Video:
                    mediaType = Environment.DirectoryMovies;
                    break;
            }
            
          
            Java.IO.File directory = null;
            if (mediaType != null)
            {
                directory = saveToAlbum ? Environment.GetExternalStoragePublicDirectory(mediaType) : context.GetExternalFilesDir(mediaType);
            }
            else
            {
                // non-media library type - files dir for caching
                directory = context.GetExternalFilesDir(null); 
            }
            using (Java.IO.File mediaStorageDir = new Java.IO.File(directory, subdir))
            {
                if (!mediaStorageDir.Exists())
                {
                    if (!mediaStorageDir.Mkdirs())
                        throw new IOException("Couldn't create directory, have you added the WRITE_EXTERNAL_STORAGE permission?");

                    if (!saveToAlbum)
                    {
                        // Ensure this media doesn't show up in gallery apps
                        using (Java.IO.File nomedia = new Java.IO.File(mediaStorageDir, ".nomedia"))
                            nomedia.CreateNewFile();
                    }
                }

                return Uri.FromFile(new Java.IO.File(GetUniquePath(mediaStorageDir.Path, name, fileType)));
            }
        }

        internal static Task<Tuple<string, bool>> GetFileForUriAsync(Context context, Uri uri, MediaFileType fileType, bool saveToAlbum)
        {
            var tcs = new TaskCompletionSource<Tuple<string, bool>>();

            Task.Factory.StartNew(() =>
            {
                string contentPath = GetFilePathForUri(context, uri);

                // If they don't follow the "rules", try to copy the file locally
                if (contentPath == null || !contentPath.StartsWith("file", StringComparison.InvariantCultureIgnoreCase))
                {
                    string fileName = null;
                    try
                    {
                        fileName = contentPath == null ? Path.GetFileName(uri.Path) : Path.GetFileName(contentPath);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine("Unable to get file path name, using new unique " + ex);
                    }


                    var outputPath = GetOutputMediaFile(context, "temp", fileName, fileType, false);

                    try
                    {
                        using (Stream input = context.ContentResolver.OpenInputStream(uri))
                        using (Stream output = File.Create(outputPath.Path))
                            input.CopyTo(output);

                        contentPath = outputPath.Path;
                    }
                    catch (Java.IO.FileNotFoundException fnfEx)
                    {
                        // If there's no data associated with the uri, we don't know
                        // how to open this. contentPath will be null which will trigger
                        // MediaFileNotFoundException.
                        System.Diagnostics.Debug.WriteLine("Unable to save picked file from disk " + fnfEx);
                    }
                }

                tcs.SetResult(new Tuple<string, bool>(contentPath, false));


            }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default);


            return tcs.Task;
        }

        // CREDIT: https://stackoverflow.com/a/36714242/325551
        public static string GetFilePathForUri(Context context, Uri uri)
        {
            String selection = null;
            String[]
            selectionArgs = null;
            var projection = new List<string> {
                    MediaStore.MediaColumns.Data, MediaStore.MediaColumns.DisplayName
                };
            if (DocumentsContract.IsDocumentUri(context.ApplicationContext, uri))
            {
               
                if (isExternalStorageDocument(uri))
                {
                    string docId = DocumentsContract.GetDocumentId(uri);
                    string[] split = docId.Split(':');
                    return Environment.ExternalStorageDirectory.Path + "/" + split[1];
                }
                else if (isDownloadsDocument(uri))
                {
                    string id = DocumentsContract.GetDocumentId(uri);
                    uri = ContentUris.WithAppendedId(
                            Uri.Parse("content://downloads/public_downloads"), int.Parse(id));
                }
                else if (isMediaDocument(uri))
                {
                    string docId = DocumentsContract.GetDocumentId(uri);
                    string[] split = docId.Split(':');
                    string type = split[0];
                    if ("image".Equals(type))
                    {
                        uri = MediaStore.Images.Media.ExternalContentUri;
                    }
                    else if ("video".Equals(type))
                    {
                        uri = MediaStore.Video.Media.ExternalContentUri;
                    }
                    else if ("audio".Equals(type))
                    {
                        uri = MediaStore.Audio.Media.ExternalContentUri;
                    }
                    projection.Add(MediaStore.MediaColumns.MimeType);
                    selection = "_id=?";
                    selectionArgs = new String[]{
                        split[1]
                    };
                }
            }

            if ("content".Equals(uri.Scheme, StringComparison.InvariantCultureIgnoreCase))
            {
                ICursor cursor = null;
                try
                {
                    cursor = context.ContentResolver
                            .Query(uri, projection.ToArray(), selection, selectionArgs, null);
                    int column_index = cursor.GetColumnIndex(MediaStore.MediaColumns.Data);
                    int displayname_index = cursor.GetColumnIndex(MediaStore.MediaColumns.DisplayName);
                    int mimetype_index = cursor.GetColumnIndex(MediaStore.MediaColumns.MimeType);
                    if (cursor.MoveToFirst())
                    {
                        string displayName = (displayname_index > -1) ? cursor.GetString(displayname_index) : "tempfilename";
                        string mimeType = (mimetype_index > -1) ? cursor.GetString(mimetype_index) : "";
                        return cursor.GetString(column_index) ?? Path.Combine("//MEDIA_PICKER", mimeType, displayName);
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine($"Error resolving picked file URI: {e.Message}");
                }
                finally
                {
                    if (cursor != null)
                    {
                        cursor.Close();
                        cursor.Dispose();
                    }
                }
            }
            else if ("file".Equals(uri.Scheme, StringComparison.InvariantCultureIgnoreCase))
            {
                return uri.Path;
            }
            return null;
        }

        public static bool isExternalStorageDocument(Uri uri)
        {
            return "com.android.externalstorage.documents".Equals(uri.Authority);
        }

        public static bool isDownloadsDocument(Uri uri)
        {
            return "com.android.providers.downloads.documents".Equals(uri.Authority);
        }

        public static bool isMediaDocument(Uri uri)
        {
            return "com.android.providers.media.documents".Equals(uri.Authority);
        }

        private static string GetLocalPath(Uri uri)
        {
            return new System.Uri(uri.ToString()).LocalPath;
        }

        private static Task<T> TaskFromResult<T>(T result)
        {
            var tcs = new TaskCompletionSource<T>();
            tcs.SetResult(result);
            return tcs.Task;
        }

        private static void OnMediaPicked(MediaPickedEventArgs e)
        {
            MediaPicked?.Invoke(null, e);
        }

        public void OnScanCompleted(string path, Uri uri)
        {
            Console.WriteLine("scan complete: " + path);
        }

        protected override void OnDestroy()
        {
            if(!completed)
            {
                DeleteOutputFile();
            }
            base.OnDestroy();
        }
    }

    internal class MediaPickedEventArgs
        : EventArgs
    {
        public MediaPickedEventArgs(int id, Exception error)
        {
            if (error == null)
                throw new ArgumentNullException("error");

            RequestId = id;
            Error = error;
            AllMedia = new List<MediaFile>();
        }

        public MediaPickedEventArgs(int id, bool isCanceled, MediaFile media = null)
        {
            RequestId = id;
            IsCanceled = isCanceled;
            if (!IsCanceled && media == null)
                throw new ArgumentNullException("media");

            Media = media;
            AllMedia = new List<MediaFile>();
            AllMedia.Add(media);
        }

        public int RequestId
        {
            get;
            private set;
        }

        public bool IsCanceled
        {
            get;
            private set;
        }

        public Exception Error
        {
            get;
            private set;
        }

        public MediaFile Media
        {
            get;
            private set;
        }

        public IList<MediaFile> AllMedia
        {
            get;
            private set;
        }

        public Task<MediaFile> ToTask()
        {
            var tcs = new TaskCompletionSource<MediaFile>();

            if (IsCanceled)
                tcs.SetResult(null);
            else if (Error != null)
                tcs.SetException(Error);
            else
                tcs.SetResult(Media);

            return tcs.Task;
        }

       
    }
}
